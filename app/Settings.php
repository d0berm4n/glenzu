<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {

	protected $fillable = ['name', 'value'];

    public static function get($name)
    {
        $set = Settings::where('name', '=', $name)->first();

        if(!empty($set))
        {
            return $set->value;
        }

        return null;
    }

    public static function set($name, $value)
    {
        $opt = Settings::where('name', '=', $name)->first();

        if(null == $opt)
        {
            $opt = new Settings();
        }

        $opt->name = $name;
        $opt->value = $value;

        $opt->save();
    }

}
