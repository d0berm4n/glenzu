<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 06.12.2015
 * Time: 12:27
 */

namespace app\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{

    public function upload(Request $request)
    {
        $filePath = 'img/gallery';

        $big = $request->file('big');
        $bigSizes = getimagesize($big);

        if(($bigSizes[0] > $bigSizes[1] && $bigSizes[0] > 1200) || ($bigSizes[1] > $bigSizes[0] && $bigSizes[1] > 1200))
        {
            return redirect('/upload')->with('error', 'Full size image biggest dimension over 1200px');
        }

        $preview = $request->file('preview');
        $previewSizes = getimagesize($preview);

        if($previewSizes[0] != 273 || $previewSizes[1] != 362)
        {
            return redirect('/upload')->with('error', 'Preview image dimensions must be 273x362px');
        }

        $big->move($filePath, $big->getClientOriginalName());

        $preview->move($filePath, $preview->getClientOriginalName());

        $image = new Image();
        $image->urlBig = $filePath . '/' . $big->getClientOriginalName();
        $image->url = $filePath . '/' . $preview->getClientOriginalName();
        $image->position = Image::all()->count() + 1;
        $image->save();

        return redirect('/dashboard');
    }

    public function  delete($id)
    {
        $img = Image::find($id);

        if(Storage::exists($img->url))
        {
            Storage::delete($img->url);
        }

        if(Storage::exists($img->urlBig))
        {
            Storage::delete($img->urlBig);
        }

        $img->delete();

        return redirect('/dashboard');
    }

    public function up($position)
    {
        if($position != 1)
        {
            $image = Image::where('position','=',$position)->firstOrFail();
            $previous = Image::where('position','=',$position - 1)->firstOrFail();

            $image->position --;
            $previous->position ++;

            $image->save();
            $previous->save();
        }

        return redirect('/dashboard');
    }

    public function down($position)
    {
        if($position != Image::all()->count())
        {
            $image = Image::where('position','=',$position)->firstOrFail();
            $next = Image::where('position','=',$position + 1)->firstOrFail();

            $image->position ++;
            $next->position --;

            $image->save();
            $next->save();
        }

        return redirect('/dashboard');
    }

}