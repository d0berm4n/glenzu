<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 06.12.2015
 * Time: 13:38
 */

namespace app\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\Exception;

class SettingsController extends Controller
{

    public function description()
    {
        return view('description');
    }

    public function saveDescription(Request $request)
    {
        Settings::set('description', $request->input('description'));
        return redirect('description');
    }

    public function email()
    {
        return view('email');
    }

    public function saveEmail(Request $request)
    {
        foreach($request->all() as $name => $value)
        {
            Settings::set($name, $value);
        }

        return redirect('/email');
    }

    public function sendMail(Request $request)
    {
        if(!empty($request->input('name'))
        && !empty($request->input('email'))
        && !empty($request->input('message'))
        && !empty($request->input('subject'))
        )
        {
            Config::set('mail.host', Settings::get('host'));
            Config::set('mail.port', Settings::get('port'));
            Config::set('mail.from.address', Settings::get('fromEmail'));
            Config::set('mail.encryption', Settings::get('encription'));
            Config::set('mail.username', Settings::get('username'));
            Config::set('mail.password', Settings::get('password'));

            $data = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'messageText' => $request->input('message')
            ];

            Mail::send('emails.welcome', $data, function($message) use ($request)
            {
                $message->to(Settings::get('toEmail'), $request->input('name'))->subject($request->input('subject'));
            });

            return redirect('/')->with('success', true);
        }
        else
        {
            return redirect('/')->with('success', false);
        }

    }

}