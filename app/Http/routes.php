<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

Route::get('/', function(){
    return view('index')->with([
        'imgs' => Image::orderBy('position', 'ASC')->get(),
        'success' => Session::pull('success')
    ]);
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/dashboard', function(){
        return view('dashboard')->with(['imgs' => Image::orderBy('position', 'ASC')->get()]);
    });

    Route::get('/upload', function(){
        return view('upload')->with(['error' => Session::pull('error')]);
    });

    Route::post('/upload', 'ImageController@upload');
    Route::get('/delete/{id}', 'ImageController@delete');
    Route::get('/up/{id}', 'ImageController@up');
    Route::get('/down/{id}', 'ImageController@down');


    Route::get('/description', 'SettingsController@description');
    Route::post('/description', 'SettingsController@saveDescription');

    Route::get('/email', 'SettingsController@email');
    Route::post('/email', 'SettingsController@saveEmail');
});



Route::post('/sendmail', 'SettingsController@sendMail');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
