<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Glenzu</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	@yield('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/lightbox.js"></script>

    {{--Uploader scripts--}}
    <script src="js/jQuery-File-Upload-9.11.2/js/vendor/jquery.ui.widget.js"></script>
    <script src="js/jQuery-File-Upload-9.11.2/js/jquery.iframe-transport.js"></script>
    <script src="js/jQuery-File-Upload-9.11.2/js/jquery.fileupload.js"></script>

    <script>
        // Anchor funct
        $('a[href^="#go"]').click(function(e){
            $('html, body').animate({
                scrollTop: $( $(this).attr('href') ).offset().top + 20
            }, 500);
            return false;
        });

    </script>
</body>
</html>
