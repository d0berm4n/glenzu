@extends('admin')

@section('content')

    @if(sizeof($imgs) > 0)
        @foreach($imgs as $img)

            <div class="col-xs-12 col-md-4 col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $img->position }}

                        <div class="btn-group">
                            <a href="/up/{{ $img->position }}" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="/down/{{ $img->position }}" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-down"></i></a>
                        </div>

                        <span class="pull-right">
                            <a onclick="deleteImg({{ $img->id }})"><i class="glyphicon glyphicon-trash"></i></a>
                        </span>

                    </div>
                    <div class="panel-body">
                        <img class="adminPreview" src="{{ url($img->url) }}">
                    </div>
                </div>
            </div>

        @endforeach
    @else
        <div class="alert alert-info" role="alert">No images uploaded yet</div>
    @endif

@endsection