<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Glenzu - administration</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container-fluid">

    <div class="row">

        <div class="col-xs-12 col-md-2 col-md-offset-2 col-sm-3">

            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="/dashboard"><i class="glyphicon glyphicon-th"></i>&nbsp;Dashboard</a></li>
                <li role="presentation"><a href="/upload"><i class="glyphicon glyphicon-upload"></i>&nbsp;Upload image</a></li>
                <li role="presentation"><a href="/description"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit description</a></li>
                <li role="presentation"><a href="/email"><i class="glyphicon glyphicon-cog"></i>&nbsp;Configure email</a></li>
                <li role="presentation"><a href="/auth/logout"><i class="glyphicon glyphicon-log-out"></i>&nbsp;Logout</a></li>
            </ul>
            {{--<div class="btn btn-primary btn-lg btn-block"></div>--}}
        </div>

        <div class="col-xs-12 col-md-6 col-sm-9">

            @yield('content')

        </div>
    </div>

</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script>

        function deleteImg(id)
        {
            if(confirm('Are you shure'))
            {
                location.assign('/delete/' + id);
            }
        }

        $(".form-horizontal").submit( function( e ) {
            var form = this;
            e.preventDefault(); //Stop the submit for now
            //Replace with your selector to find the file input in your form

            var big = getImageSize($('#big')),
                lil =  getImageSize($('#preview'));

            if(null != big && null != lil)
            {
                if (big.width > big.height > 1200 || big.height > big.width > 1200)
                {
                    alert('Full size image biggest side size must be under 1200px');
                }
                else
                {
                    if (lil.width == 273 && lil.height == 362)
                    {
                        form.submit();
                    }
                    else
                    {
                        alert('Preview image dimensions must be 273x362px');
                    }
                }
            }
            else
            {
                form.submit();
            }
        });

        function getImageSize(input)
        {
            var width = 0,
                height = 0,
                file = input.files && input.files[0];

            if( file )
            {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );

                img.onload = function() {
                    width = img.naturalWidth,
                    height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );
                }

                return {
                    width: width,
                    height: height
                }
            }
            else
            {
                return null;
            }
        }

    </script>

</body>
</html>