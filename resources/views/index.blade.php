@extends('app')

@section('content')

<!-- Header -->
<header>
    <div class="container">
        <div class="header-inner">

            <div class="header-links">
                <a href="#goFooter"><img src="img/icons/message.png" alt=""></a>
                <a href="#goDescription"><img src="img/icons/info.png" alt=""></a>
            </div>
            <div class="header-pattern"></div>

        </div>

        <div class="header-sign"><img src="img/sign.png" alt=""></div>

        <div class="main-title">
            <h2>Not everything<span><img src="img/icons/brackets.png" alt=""></span></h2>
            <h1>is black and white</h1>
        </div>

    </div>
</header>
<!-- /Header -->

<!-- Content -->
<div class="content gallery-content">
    <div class="container">

        <!-- Gallery -->
        <div class="row gallery">

            @if(isset($imgs))
                @foreach($imgs as $img)
                    @include('img', ['img' => $img])
                @endforeach
            @endif

        <!-- /Gallery -->

        <div class="row">
            <div class="col-sm-12">
                <div class="gallery-description" id="goDescription">
                    <p><span>Not everything is black and White</span> {{ \App\Settings::get('description') }}</p>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /Content -->

<!-- Footer -->
<footer id="goFooter">
    <div class="container">

        <h5>Questions?</h5>
        <p class="subtitle">For any inquiries or questions feel free to send the Author an email</p>

        <form action="/sendmail" method="post">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-areas">
                <div class="form-left">
                    <div class="areas-holder">
                        <i><img src="img/icons/name.png" alt=""></i>
                        <input type="text" placeholder="Name" name="name" required>
                    </div>
                    <div class="areas-holder">
                        <i><img src="img/icons/email.png" alt=""></i>
                        <input type="email" placeholder="Email" name="email" required>
                    </div>
                    <div class="areas-holder">
                        <i><img src="img/icons/subject.png" alt=""></i>
                        <input type="text" placeholder="Subject" name="subject" required>
                    </div>
                </div>

                <div class="form-right">
                    <div class="areas-holder">
                        <i><img src="img/icons/clip.png" alt=""></i>
                        <textarea placeholder="Message" name="message" required></textarea>
                    </div>
                </div>
            </div>


            <div class="btn-holder">
                <button type="submit" class="btn btn-red">Contact Author <i><img src="img/icons/plane.png" alt=""></i></button>
            </div>

            @if($success !== null)

                @if( $success )
                    <div class="notif success"><img src="img/icons/success.png" alt="">Thank you, we have received your message, we will getback to you within one day!</div>
                @else
                    <div class="notif error"><img src="img/icons/error.png" alt="">Sorry, something went wront. Did you fill out all the fields? You should try again!</div>
                @endif

            @endif

        </form>

    </div>
</footer>

@endsection
