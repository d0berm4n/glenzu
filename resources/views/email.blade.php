@extends('admin')
@section('content')

    <form id="email" action="/email" method="post" class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-2 control-label from">From:</label>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="host" class="col-sm-2 control-label">Host</label>
            <div class="col-sm-10">
                <input  id="host" type="text" name="host" value="{{ \App\Settings::get('host') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="port" class="col-sm-2 control-label">Port</label>
            <div class="col-sm-10">
                <input id="port"  type="text" name="port" value="{{ \App\Settings::get('port') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="fromEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input id="fromEmail"  type="email" name="fromEmail" value="{{ \App\Settings::get('fromEmail') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="fromName" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input id="fromName"  type="text" name="fromName" value="{{ \App\Settings::get('fromName') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="username"  class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
                <input id="username"  type="text" name="username" value="{{ \App\Settings::get('username') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="password"  class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input id="password"  type="password" name="password" value="{{ \App\Settings::get('password') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="encription"  class="col-sm-2 control-label">Encription</label>
            <div class="col-sm-10">
                <input id="encription"  type="text" name="encription" value="{{ \App\Settings::get('encription') }}" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label from">To:</label>
        </div>

        <div class="form-group">
            <label for="toEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input id="toEmail"  type="email" name="toEmail" value="{{ \App\Settings::get('toEmail') }}" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

    </form>

@endsection