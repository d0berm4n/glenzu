@extends('admin')

@section('content')

    @if( !empty($error) )

        <div class="alert alert-danger" role="alert"><i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;{{ $error }}</div>

    @endif

    <form class="form-horizontal" action="/upload" method="post" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="big" class="col-sm-2 control-label">Fullsize</label>
            <div class="col-sm-10">
                <input id="big" type="file" name="big" accept="image/jpeg,image/png" required>
                <p class="help-block">1200px max on biggest side</p>
            </div>
        </div>

        <div class="form-group">
            <label for="preview" class="col-sm-2 control-label">Preview</label>
            <div class="col-sm-10">
                <input id="preview" type="file" name="preview" accept="image/jpeg,image/png" required>
                <p class="help-block">273x362px</p>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Upload</button>
            </div>
        </div>

    </form>

@endsection