@extends('admin')

@section('content')
    <form action="/description" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="description">Description</label>
            <textarea id="description" class="admin-description" name="description">{{ \App\Settings::get('description') }}</textarea>
        </div>

        <div class="form-group">
            <div class="">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

    </form>

@endsection