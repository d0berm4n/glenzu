<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 04.12.15
 * Time: 11:41
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Image;

class ImagesTableSeeder extends Seeder {

    public function run()
    {
        for($i = 1; $i < 7; $i++ )
        {
            $img = new Image;
            $img->url = "img/gallery/" . $i . ".jpg";
            $img->urlBig = "img/gallery/" . $i . "-large.jpg";
            $img->position = $i;
            $img->save();
        }
    }

}